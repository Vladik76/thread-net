import { Injectable, OnDestroy } from '@angular/core';
import { ConfirmationDialogComponent } from '../components/confirmation-dialog/confirmation-dialog.component'
import { MatDialog } from '@angular/material/dialog';
import { map, takeUntil } from 'rxjs/operators';
import { Subscription, Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ConfirmationDialogService implements OnDestroy {
    private unsubscribe$ = new Subject<void>();

    public constructor(private dialog: MatDialog) { }

    public openConfirmationDialog(confirmMessage: string, callback: Function) {
        const dialog = this.dialog.open(ConfirmationDialogComponent, {
            data: { confirmMessage: confirmMessage },
            minWidth: 300,
            autoFocus: true,
            backdropClass: 'dialog-backdrop',
            position: {
                top: '20px'
            }
        });

        dialog
            .afterClosed()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((result) => {
                callback(result);
            });
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
