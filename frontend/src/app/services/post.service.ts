import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { NewPost } from '../models/post/new-post';
import { User } from '../models/user';

@Injectable({ providedIn: 'root' })
export class PostService {
    public routePrefix = '/api/posts';

    constructor(private httpService: HttpInternalService) { }

    public getPosts() {
        return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}`);
    }

    public createPost(post: NewPost) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}`, post);
    }
    public updatePost(post: Post) {
        return this.httpService.putFullRequest<Post>(`${this.routePrefix}`, post);
    }
    public deletePost(id: number) {
        return this.httpService.deleteFullRequest(`${this.routePrefix}/` + id);
    }

    public likePost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}/like`, reaction);
    }

    public SharePost(post: Post, email: string) {
        post.author.email = email;
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}/share`, post);
    }
}
