import { Injectable, OnDestroy } from '@angular/core';
import { ShareDialogComponent } from '../components/share-dialog/share-dialog.component';
import { Post } from '../models/post/post';
import { MatDialog } from '@angular/material/dialog';
import { map, takeUntil } from 'rxjs/operators';
import { AuthenticationService } from './auth.service';
import { Subscription, Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ShareDialogService implements OnDestroy {
    private unsubscribe$ = new Subject<void>();

    public constructor(private dialog: MatDialog) { }

    public openShareDialog(post: Post) {
        const dialog = this.dialog.open(ShareDialogComponent, {
            data: { post: post },
            minWidth: 300,
            autoFocus: true,
            backdropClass: 'dialog-backdrop',
            position: {
                top: '20px'
            }
        });


    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

}
