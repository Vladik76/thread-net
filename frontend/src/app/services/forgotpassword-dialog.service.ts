import { Injectable, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { ForgotPasswordDialogComponent } from '../components/forgotpassword-dialog/forgotpassword-dialog.component';

@Injectable({ providedIn: 'root' })
export class ForgotPasswordDialogService implements OnDestroy {
    private unsubscribe$ = new Subject<void>();

    public constructor(private dialog: MatDialog) { }

    public openForgotPasswordDialog() {
        const dialog = this.dialog.open(ForgotPasswordDialogComponent, {
            minWidth: 300,
            autoFocus: true,
            backdropClass: 'dialog-backdrop',
            position: {
                top: '20px'
            }
        });

    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}
