import { Component, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { UserService } from '../../services/user.service';
import { SnackBarService } from '../../services/snack-bar.service';
import { Location } from '@angular/common';
import { ResetPasswordDTO } from 'src/app/models/auth/reset-password-dto';
import { AuthenticationService } from 'src/app/services/auth.service';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { OnInit } from '@angular/core';

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.sass']
})
export class ResetPasswordComponent implements OnInit, OnDestroy {
    private _token: string;
    private _email: string;
    public password: string;
    public confirm: string;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private userService: UserService,
        private snackBarService: SnackBarService,
        private location: Location,
        private authenticationService: AuthenticationService,
        private _route: ActivatedRoute
    ) { }

    public ngOnInit() {
        this._token = this._route.snapshot.queryParams['token'];
        this._email = this._route.snapshot.queryParams['email'];
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public saveNewInfo() {
        if (this.password != null && this.confirm != null) {
            if (this.password == this.confirm) {
                const resetPassDto: ResetPasswordDTO = {
                    email: this._email,
                    token: this._token,
                    password: this.password,
                    confirmPassword: this.confirm
                }
                const passwordChanged = this.authenticationService.resetPassword(resetPassDto);


                passwordChanged.pipe(takeUntil(this.unsubscribe$)).subscribe(
                    () => {
                        this.snackBarService.showUsualMessage("Password has succesfully changed");
                        this.goBack();
                    },
                    (error) => this.snackBarService.showErrorMessage(error)
                );
            }
            else {
                this.snackBarService.showErrorMessage("Passwrod and Confirm Password do not match");
            }
        }
        else {
            this.snackBarService.showErrorMessage("Both Password and Confirm must be entered on the form");
        }
    }

    public goBack = () => { this.location.go("/"); window.location.reload(); }
}
