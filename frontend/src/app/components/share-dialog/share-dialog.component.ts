import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { SnackBarService } from '../../services/snack-bar.service';
import { PostService } from 'src/app/services/post.service';
import { takeUntil } from 'rxjs/operators';
import { FormGroup, Validators } from '@angular/forms';

@Component({
    templateUrl: './share-dialog.component.html',
    styleUrls: ['./share-dialog.component.sass']
})
export class ShareDialogComponent implements OnDestroy {

    public email: string;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private dialogRef: MatDialogRef<ShareDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private snackBarService: SnackBarService,
        private postService: PostService,

    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public close() {
        this.dialogRef.close(false);

    }

    public share() {
        if (this.email != null) {

            const postShare = this.postService.SharePost(this.data.post, this.email);


            postShare.pipe(takeUntil(this.unsubscribe$)).subscribe(
                () => {
                    this.snackBarService.showUsualMessage("Message has successfully sent.");
                    this.dialogRef.close(false);
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
        }
        else {
            this.snackBarService.showErrorMessage("Please enter email");
        }
    }

}
