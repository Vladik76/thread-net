import { Component, Input, OnDestroy } from '@angular/core';
import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { ConfirmationDialogService } from '../../services/confirmation-dialog.service';
import { ShareDialogService } from '../../services/share-dialog.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { PostService } from 'src/app/services/post.service';
import { GyazoService } from 'src/app/services/gyazo.service';
import { MainThreadComponent } from '../main-thread/main-thread.component';


@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnDestroy {
    @Input() public post: Post;
    @Input() public currentUser: User;

    public showComments = false;
    public newComment = {} as NewComment;
    public editPost = false;
    public imageUrl: string;
    public imageFile: File;
    public loading = false;
    public wholiked = "";

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private postService: PostService,
        private gyazoService: GyazoService,
        private confDialogService: ConfirmationDialogService,
        private shareDialogService: ShareDialogService,
        private MainThreadComponent: MainThreadComponent

    ) { }

    public ToggleEditPost() {
        this.editPost = !this.editPost;
        if (this.editPost) {
            this.imageUrl = this.post.previewImage;
        }
        else {
            this.postService
                .getPosts()
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe(
                    (resp) => {
                        this.post = resp.body.find(i => i.id === this.post.id);

                    },
                    (error) => this.snackBarService.showErrorMessage(error)
                );
        }
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }

    public likePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likePost(this.post, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.likeService
            .likePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));
    }

    public dislikePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.dislikePost(this.post, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.likeService
            .dislikePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }

    public sendPost() {
        if (this.imageUrl == undefined) {
            this.post.previewImage = undefined;
        }
        const postSubscription = !this.imageFile
            ? this.postService.updatePost(this.post)
            : this.gyazoService.uploadImage(this.imageFile).pipe(
                switchMap((imageData) => {
                    this.post.previewImage = imageData.url;
                    return this.postService.updatePost(this.post);
                })
            );

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            () => {
                this.editPost = false;
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );

    }

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.imageUrl = undefined;
        this.imageFile = undefined;
    }

    public DeletePost() {
        var callback = (result: boolean): void => {
            if (result) {
                const postSubscription = this.postService.deletePost(this.post.id);
                postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
                    () => {
                        this.MainThreadComponent.getPosts();
                    },
                    (error) => this.snackBarService.showErrorMessage(error)
                );
            }
        }
        this.confDialogService.openConfirmationDialog(`Are you sure?`, callback);
    }

    public GetLikes() {
        return this.post.reactions.filter(x => x.isLike === true).length;
    }

    public GetDisLikes() {
        return this.post.reactions.filter(x => x.isLike === false).length;

    }

    public WhoLiked() {
        this.wholiked = "";
        this.post.reactions.filter(x => x.isLike === true).forEach(x => {
            this.wholiked += x.user.userName + " \n ";
        });
    }

    public sharePost() {
        this.shareDialogService.openShareDialog(this.post);
    }
}
