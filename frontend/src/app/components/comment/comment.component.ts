import { Component, Input } from '@angular/core';
import { empty, Observable, Subject } from 'rxjs';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { DialogType } from 'src/app/models/common/auth-dialog-type';
import { User } from 'src/app/models/user';
import { AuthDialogService } from 'src/app/services/auth-dialog.service';
import { AuthenticationService } from 'src/app/services/auth.service';
import { CommentService } from 'src/app/services/comment.service';
import { ConfirmationDialogService } from 'src/app/services/confirmation-dialog.service';
import { LikeService } from 'src/app/services/like.service';
//import { PostService } from 'src/app/services/post.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { Comment } from '../../models/comment/comment';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent {
    @Input() public comment: Comment;
    @Input() public currentUser: User;

    public editComment = false;
    public wholiked = "";

    private unsubscribe$ = new Subject<void>();
    MainThreadComponent: any;

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private confDialogService: ConfirmationDialogService,
        //private postService: PostService
    ) { }

    private oldBody: string;


    public ToggleEditComment() {
        this.editComment = !this.editComment;
        if (this.editComment) {
            this.oldBody = this.comment.body;
        }
        else {
            this.comment.body = this.oldBody;
            this.unsubscribe$;
        }
    }

    public DeleteComment() {
        var callback = (result: boolean): void => {
            if (result) {
                const postSubscription = this.commentService.deleteComment(this.comment.id);
                postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
                    () => {
                        this.MainThreadComponent.getPosts();
                    },
                    (error) => this.snackBarService.showErrorMessage(error)
                );
            }
        }
        this.confDialogService.openConfirmationDialog(`Are you sure?`, callback);
    }

    public likeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }

        this.likeService
            .likeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }

    public dislikeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.dislikeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }

        this.likeService
            .dislikeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }

    public sendComment() {

        this.commentService
            .updateComment(this.comment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                () => {
                    this.editComment = false;
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public GetLikes() {
        return this.comment.reactions.filter(x => x.isLike === true).length;
    }

    public GetDisLikes() {
        return this.comment.reactions.filter(x => x.isLike === false).length;

    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public WhoLiked() {
        this.wholiked = "";
        this.comment.reactions.filter(x => x.isLike === true).forEach(x => {
            this.wholiked += x.user.userName + " \n ";
        });
    }
}
