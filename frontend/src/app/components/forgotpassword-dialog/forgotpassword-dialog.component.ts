import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { SnackBarService } from '../../services/snack-bar.service';
import { takeUntil } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/services/auth.service';
import { ForgotPasswordDto } from 'src/app/models/auth/forgot-password-dto';

@Component({
    templateUrl: './forgotpassword-dialog.component.html',
    styleUrls: ['./forgotpassword-dialog.component.sass']
})
export class ForgotPasswordDialogComponent implements OnDestroy {

    public email: string;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private dialogRef: MatDialogRef<ForgotPasswordDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private snackBarService: SnackBarService,
        private authenticationService: AuthenticationService
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public close() {
        this.dialogRef.close(false);

    }

    public sendLink() {
        if (this.email != null) {
            const forgotPassDto: ForgotPasswordDto = {
                email: this.email,
                clientURI: 'http://localhost:4200/resetpassword'
            }
            const forgotPasswordSend = this.authenticationService.sendResetPasswordLink(forgotPassDto);


            forgotPasswordSend.pipe(takeUntil(this.unsubscribe$)).subscribe(
                () => {
                    this.snackBarService.showUsualMessage("Email with link has successfully sent.");
                    this.dialogRef.close(false);
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
        }
        else {
            this.snackBarService.showErrorMessage("Please enter correct email address");
        }
    }

}
