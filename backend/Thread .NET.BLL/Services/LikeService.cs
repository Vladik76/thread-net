﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.DAL.Context;

namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        private readonly EmailService _emailService;
        private readonly IHubContext<PostHub> _postHub;

        public LikeService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub, IOptions<EmailService> emailService) : base(context, mapper) {
            _postHub = postHub;
            _emailService = emailService.Value;
        }

        public async Task LikePost(NewReactionDTO reaction)
        {
            var likes = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId);

            if (likes.Any())
            {
                _context.PostReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();

                return;
            }

            _context.PostReactions.Add(new DAL.Entities.PostReaction
            {
                PostId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });
            var user=await _context.Users
                .Include(u => u.Avatar)
                .FirstOrDefaultAsync(u => u.Id == reaction.UserId);
            var post= await _context.Posts
                 .FirstOrDefaultAsync(p => p.Id == reaction.EntityId);
            var PostAuthor = post.Author;

            await _context.SaveChangesAsync();
            await _postHub.Clients.All.SendAsync("Liked",reaction);
            await _emailService.SendEmailAsync(PostAuthor.Email, "Your post has recently liked", "Your post<br><strong>"+post.Body+"</strong><br> has recently liked by "+user.UserName);
        }

        public async Task LikeComment(NewReactionDTO reaction)
        {
            var likes = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId);

            if (likes.Any())
            {
                _context.CommentReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();

                return;
            }

            _context.CommentReactions.Add(new DAL.Entities.CommentReaction
            {
                CommentId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();
        }
    }
}
