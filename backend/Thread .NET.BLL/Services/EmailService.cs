﻿using MimeKit;
using MailKit.Net.Smtp;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.DAL.Context;
using AutoMapper;
using Microsoft.Extensions.Options;
using System;

namespace Thread_.NET.BLL.Services
{
    public sealed class EmailService 
    {
        public EmailService() { }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress(SmtpFromName, SmtpFromEmail));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };

            using var client = new SmtpClient();
            await client.ConnectAsync(SmtpServer, SmtpPort, SmtpSecure);
            await client.AuthenticateAsync(SmtpUserName, SmtpPassword);
            await client.SendAsync(emailMessage);

            await client.DisconnectAsync(true);
        }

         public string SmtpServer { get; set; }

        public int SmtpPort { get; set; }

        public string SmtpUserName { get; set; }

        public string SmtpPassword { get; set; }

        public bool SmtpSecure { get; set; }

        public string SmtpFromEmail { get; set; }

        public string SmtpFromName { get; set; }
    }
}
