﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;
        private readonly EmailService _emailService;

        public PostService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub, IOptions<EmailService> emailService) : base(context, mapper)
        {
            _postHub = postHub;
            _emailService = emailService.Value;
        }

        public async Task<ICollection<PostDTO>> GetAllPosts()
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments.Where(c=>c.IsDeleted==false))
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .Where(p => p.IsDeleted == false)
                .OrderByDescending(post => post.CreatedAt)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<ICollection<PostDTO>> GetAllPosts(int userId)
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Comments.Where(c => c.IsDeleted == false))
                    .ThenInclude(comment => comment.Author)
                .Where(p => p.AuthorId == userId) // Filter here
                .Where(p=>p.IsDeleted==false)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> CreatePost(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPost = await _context.Posts
                .Include(post => post.Author)
					.ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);

            var createdPostDTO = _mapper.Map<PostDTO>(createdPost);
            await _postHub.Clients.All.SendAsync("NewPost", createdPostDTO);
            return createdPostDTO;
        }

        public async Task UpdatePost(PostDTO postDTO)
        {
            var postEntity = await GetPostByIdInternal(postDTO.Id);
            if (postEntity == null)
            {
                throw new NotFoundException(nameof(Post), postDTO.Id);
            }

            var timeNow = DateTime.Now;

            postEntity.Body = postDTO.Body;
            postEntity.UpdatedAt = timeNow;

            if (!string.IsNullOrEmpty(postDTO.PreviewImage))
            {
                if (postEntity.Preview == null)
                {
                    postEntity.Preview = new Image
                    {
                        URL = postDTO.PreviewImage
                    };
                }
                else
                {
                    postEntity.Preview.URL = postDTO.PreviewImage;
                    postEntity.Preview.UpdatedAt = timeNow;
                }
            }
            else
            {
                if (postEntity.Preview != null)
                {
                    _context.Images.Remove(postEntity.Preview);
                }
            }

            _context.Posts.Update(postEntity);
            await _context.SaveChangesAsync();
        }

        public async Task DeletePost(int id,int AuthorId)
        {
            
            var postEntity = await GetPostByIdInternal(id);
            if (postEntity == null)
            {
                throw new NotFoundException(nameof(Post),id);
            }
            if (postEntity.AuthorId == AuthorId)
            {
                var timeNow = DateTime.Now;

                postEntity.IsDeleted = true;

                _context.Posts.Update(postEntity);
                await _context.SaveChangesAsync();
            }
        }

        public async Task SharePost(PostDTO postDTO)
        {
            string body = postDTO.Body;
            string imageURL = postDTO.PreviewImage;
            
            if(!String.IsNullOrEmpty(imageURL))
            {
                body = "<strong>Author:</strong>"+postDTO.Author.UserName+"<br><img src='" + imageURL + "'><br>" + body;
            }
            else
            {
                body = "<strong>Author:</strong>" + postDTO.Author.UserName + "<br>" + body;
            }
               await _emailService.SendEmailAsync(postDTO.Author.Email, "Post Shared", body);
        }

        private async Task<Post> GetPostByIdInternal(int id)
        {
            return await _context.Posts
                .Include(p => p.Preview)
                .FirstOrDefaultAsync(p => p.Id == id);
        }


    }
}
