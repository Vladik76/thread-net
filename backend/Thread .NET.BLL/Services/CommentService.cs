﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        public CommentService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            return _mapper.Map<CommentDTO>(createdComment);
        }

        public async Task UpdateComment(CommentDTO commentDTO)
        {
            var commentEntity = await GetCommentByIdInternal(commentDTO.Id);
            if (commentEntity == null)
            {
                throw new NotFoundException(nameof(Comment), commentDTO.Id);
            }

            var timeNow = DateTime.Now;

            commentEntity.Body = commentDTO.Body;
            commentEntity.UpdatedAt = timeNow;
            _context.Comments.Update(commentEntity);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteComment(int id, int AuthorId)
        {

            var commentEntity = await GetCommentByIdInternal(id);
            if (commentEntity == null)
            {
                throw new NotFoundException(nameof(Comment), id);
            }
            if (commentEntity.AuthorId == AuthorId)
            {
                var timeNow = DateTime.Now;

                commentEntity.IsDeleted = true;

                _context.Comments.Update(commentEntity);
                await _context.SaveChangesAsync();
            }
        }

        private async Task<Comment> GetCommentByIdInternal(int id)
        {
            return await _context.Comments
                .FirstOrDefaultAsync(p => p.Id == id);
        }
    }
}
